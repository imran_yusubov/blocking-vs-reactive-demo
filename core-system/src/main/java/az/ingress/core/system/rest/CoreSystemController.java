package az.ingress.core.system.rest;

import az.ingress.core.system.dto.CardDto;
import az.ingress.core.system.dto.Slowness;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/core-system")
public class CoreSystemController {

    public Long slowness = 0L;

    @GetMapping("/slowness")
    public Long getSlowness() {
        return slowness;
    }

    @PostMapping("/slowness")
    public Long setSlowness(@RequestBody Slowness slowness) {
        log.trace("Slowness {}", slowness);
        this.slowness = slowness.getSlowness();
        return this.slowness;
    }

    @GetMapping
    public CardDto getCards() throws InterruptedException {
        if (slowness > 0)
            Thread.sleep(slowness);
        return new CardDto();
    }

}
