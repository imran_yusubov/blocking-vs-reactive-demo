package az.ingress.core.system.dto;

import lombok.Data;

@Data
public class Slowness {

    private Long slowness;
}
