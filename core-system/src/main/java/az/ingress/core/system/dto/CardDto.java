package az.ingress.core.system.dto;

import lombok.Data;

@Data
public class CardDto {

    private Long id = 3863L;
    private String name = "A Master Card from Core System";
}
